package com.eliseev.pet.project.taskmanager.service

import com.eliseev.pet.project.taskmanager.entity.User
import com.eliseev.pet.project.taskmanager.repository.UserRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.Optional
import java.util.UUID

@Service
@Transactional(readOnly = true)
class UserService(private val userRepository: UserRepository) {

    fun findByUsername(username: String): Optional<User> {
        return userRepository.findByUsername(username)
    }

    @Transactional
    fun save(user: User): User {
        return userRepository.save(user)
    }

    fun findAll(): List<User> {
        return userRepository.findAll()
    }

    fun findById(id: UUID): Optional<User> {
        return userRepository.findById(id)
    }

    @Transactional
    fun deleteById(id: UUID) {
        userRepository.deleteById(id)
    }
}