package com.eliseev.pet.project.taskmanager.dto

enum class TaskPriority {
    LOW, MEDIUM, HIGH
}