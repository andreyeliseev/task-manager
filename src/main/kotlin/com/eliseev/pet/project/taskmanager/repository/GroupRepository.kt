package com.eliseev.pet.project.taskmanager.repository

import com.eliseev.pet.project.taskmanager.entity.Group
import org.springframework.data.jpa.repository.JpaRepository
import java.util.Optional
import java.util.UUID

interface GroupRepository : JpaRepository<Group, UUID> {
    fun findByName(name: String): Optional<Group>
}