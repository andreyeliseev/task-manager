package com.eliseev.pet.project.taskmanager.repository

import com.eliseev.pet.project.taskmanager.entity.User
import org.springframework.data.jpa.repository.JpaRepository
import java.util.Optional
import java.util.UUID

interface UserRepository : JpaRepository<User, UUID> {
    fun findByUsername(username: String): Optional<User>
}