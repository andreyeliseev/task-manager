package com.eliseev.pet.project.taskmanager.service

import com.eliseev.pet.project.taskmanager.entity.Group
import com.eliseev.pet.project.taskmanager.repository.GroupRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.Optional
import java.util.UUID

@Service
@Transactional(readOnly = true)
class GroupService(private val groupRepository: GroupRepository) {

    fun findByName(name: String): Optional<Group> {
        return groupRepository.findByName(name)
    }

    @Transactional
    fun save(group: Group): Group {
        return groupRepository.save(group)
    }

    fun findAll(): List<Group> {
        return groupRepository.findAll()
    }

    fun findById(id: UUID): Optional<Group> {
        return groupRepository.findById(id)
    }

    @Transactional
    fun deleteById(id: UUID) {
        groupRepository.deleteById(id)
    }
}