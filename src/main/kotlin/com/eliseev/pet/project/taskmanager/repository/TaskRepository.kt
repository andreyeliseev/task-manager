package com.eliseev.pet.project.taskmanager.repository

import com.eliseev.pet.project.taskmanager.entity.Task
import org.springframework.data.jpa.repository.JpaRepository
import java.util.UUID

interface TaskRepository : JpaRepository<Task, UUID>