package com.eliseev.pet.project.taskmanager.dto

enum class TaskStatus {
    TODO, IN_PROGRESS, COMPLETED
}