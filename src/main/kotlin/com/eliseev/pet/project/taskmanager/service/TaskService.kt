package com.eliseev.pet.project.taskmanager.service

import com.eliseev.pet.project.taskmanager.entity.Task
import com.eliseev.pet.project.taskmanager.repository.TaskRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.Optional
import java.util.UUID

@Service
@Transactional(readOnly = true)
class TaskService(private val taskRepository: TaskRepository) {

    @Transactional
    fun save(task: Task): Task {
        return taskRepository.save(task)
    }

    fun findAll(): List<Task> {
        return taskRepository.findAll()
    }

    fun findById(id: UUID): Optional<Task> {
        return taskRepository.findById(id)
    }

    @Transactional
    fun deleteById(id: UUID) {
        taskRepository.deleteById(id)
    }
}