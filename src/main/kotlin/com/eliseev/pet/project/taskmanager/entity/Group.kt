package com.eliseev.pet.project.taskmanager.entity

import org.hibernate.annotations.GenericGenerator
import java.util.*
import jakarta.persistence.*

@Entity
data class Group(
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    val id: UUID = UUID.randomUUID(),

    @Column(nullable = false, unique = true)
    val name: String,

    @ManyToMany(mappedBy = "groups")
    val users: MutableSet<User> = mutableSetOf(),

    @OneToMany(mappedBy = "group", cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    val tasks: MutableSet<Task> = mutableSetOf()
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Group

        return id == other.id
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}
